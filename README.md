# Sorting Algorithms - Orange Coast College CS A200 (Data Structures) Lab #4

Lab #4 for CS A200 (Data Structures & Algorithms) at Orange Coast College, Fall 2019. The class was divided up into teams, each of which had to implement one of six sorting algorithms, plus an interface which would test each one on a randomly generated list of integers. The project was assigned on 10/30/2019, and was submitted 11/6/2019. As with all other assignments in the class, the language used is C++, and this repository is provided as a VisualStudio 2019 solution. SortingAlgorithms.cpp contains the main() function.

This section of CS A200 was taught by Gabriela Ernsberger.

**Project leader/coordinator, bug fixes:** Hagen Atkeson

**Adaptation of group code to interface, bug fixes:** Kevin Buchik

## Teams (group leaders listed first)

### Interface
Kevin Buchik  
Junwei Qu  
Nam Bui  

### Bubble Sort
Chris Panella  
Usman Qazi  
Adam Ash  
Thien La  

### Insertion Sort
Raileen DelRosario  
Hoang Nguyen  
Michael Nguyen  
Kyle Aguilera  
Hang Chau  

### Selection Sort
Eric Bruhns  
Aaron Chao  
Carlos Castaneda  
David Sullivan  
Christian Perez  
Albert Castro  

### Merge Sort
Michael Chhun  
Anthony Ho  
Huy Hoang  

### Quick Sort
Cody Mangham  
Junwei Chen  
Albert Nguyen  
Nguyen Nguyen  

