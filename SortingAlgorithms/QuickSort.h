/*
	Mangham, Cody
	Chen, Junwei
	Nguyen, Albert
	Nguyen, Nguyen

	October 30, 2019

	CS A200
	Lab 4 (Quick Sort)
*/

#ifndef QUICKSORT_H
#define QUICKSORT_H

#include <vector>
#include <string>
#include <iostream>

using namespace std;

int quickSort(vector<int>&);
void quickSort(vector<int>& v1, int h, int k, int& count);
int partition(vector<int>&, int, int, int&);

#endif