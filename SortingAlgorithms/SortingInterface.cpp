/*
	Buchik, Kevin
	Qu, Jinping
	Bui, Nam

	October 30, 2019

	CS A200
	Lab 4 (Interface)
*/

#include "SortingInterface.h"

SortingInterface::SortingInterface()
{
}

SortingInterface::~SortingInterface()
{
}

void SortingInterface::testAll()
{
	int numints;
	cout << "Number of integers to sort: ";
	cin >> numints;
	if (numints <= 0)
		return;
	
	vector<int> unsorted;
	for (int i = 0; i < numints; ++i)
	{
		int n = rand() % SORT_MAX + 1;
		unsorted.push_back(n);
	}
	cout << "Unsorted list: ";
	printVector(unsorted);

	cout << endl << "---BUBBLE SORT---" << endl;
	testAlgorithm(bubbleSort, unsorted);

	cout << endl << "---BUCKET SORT---" << endl;
	testAlgorithm(bsWrapper, unsorted);

	cout << endl << "---INSERTION SORT---" << endl;
	testAlgorithm(insertionSort, unsorted);

	cout << endl << "---MERGE SORT---" << endl;
	testAlgorithm(mergeSort, unsorted);

	cout << endl << "---QUICK SORT---" << endl;
	testAlgorithm(quickSort, unsorted);

	cout << endl << "---SELECTION SORT---" << endl;
	testAlgorithm(selectionSort, unsorted);

	cout << endl;
}

void SortingInterface::testAlgorithm(int (*sortFunction)(vector<int>&), vector<int>& v)
{
	vector<int> vect(v);
	int cmp = sortFunction(vect);
	cout << "Sorted list: ";
	printVector(vect);
	cout << "Number of comparisons: " << cmp << endl;
	string success = "FALSE";
	if (isSorted(vect))
		success = "TRUE";
	cout << "Vector sorted successfully: " << success << endl;
}

int SortingInterface::stdlibSort(vector<int>& v)
{
	sort(v.begin(), v.end());
	return 0;
}

void SortingInterface::printVector(const vector<int>& v)
{
	for (auto itr = v.begin(); itr < v.end(); ++itr)
	{
		cout << *itr << " ";
	}
	cout << endl;
}

bool SortingInterface::isSorted(const vector<int>& v)
{
	for (int i = 0; i < v.size() - 1; ++i)
	{
		if (v[i + 1] < v[i])
			return false;
	}
	return true;
}