/*
	DelRosario, Raileen
	Nguyen, Hoang
	Nguyen, Michael
	Aguilera, Kyle
	Chau, Hang

	October 30, 2019

	CS A200
	Lab 4 (Insertion Sort)
*/

#include "InsertionSort.h" 

int insertionSort(vector<int>& v)
{
	int j, value;
	int count = 0;

	for (int i = 1; i < v.size(); i++)
	{
		value = v[i];
		for (j = i - 1; j >= 0 && v[j] > value; j--)
		{
			v[j + 1] = v[j];
			count++;
		}
		count++;
		v[j + 1] = value;
	}

	return count;
}