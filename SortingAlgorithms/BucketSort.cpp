/*
	Nguyen, Huy
	Nguyen, Khoa
	Khong, Brian
	Chen, Linlin
	Brunelle, Taylor
	Kriesel, Michael

	October 30, 2019

	CS A200
	Lab 4: Bucket Sort
*/

#include "BucketSort.h"

// For some reason, SortingInterface.cpp couldn't resolve the bucketSort method, but this one works fine
int bsWrapper(vector<int>& v)
{
	return bucketSort(v);
}

AnyList::AnyList()
{
	first = last = nullptr;
	count = 0;
}

AnyList::~AnyList()
{
	while (first != nullptr)
	{
		Node* temp = first;
		first = first->getNext();
		delete temp;
		temp = nullptr;
	}
}

int AnyList::insertInOrder(AnyList& list, int elem)
{
	int count = 0;
	if (list.getFirst() == nullptr)
	{
		list.setFirst(new Node(elem, nullptr));
		list.setLast(last = first);
	}
	else
	{
		if (list.getLast()->getData() <= elem)
		{
			list.getLast()->setNext(new Node(elem, nullptr));
			list.setLast(list.getLast()->getNext());
			count = 1;
		}
		else if (list.getFirst()->getData() >= elem)
		{
			list.setFirst(new Node(elem, first));
			count = 2;
		}
		else
		{
			count = 2;
			Node* leadCurrent = list.getFirst()->getNext(), 
				* tailCurrent = list.getFirst();
			bool found = false;

			while (!found && leadCurrent != list.getLast())
			{
				++count;
				if (leadCurrent->getData() >= elem)
				{
					found = true;
				}
				else
				{
					if (leadCurrent != last)
					{
						leadCurrent = leadCurrent->getNext();
						tailCurrent = tailCurrent->getNext();
					}
					else
						found = true;
				}
			}
			tailCurrent->setNext(new Node(elem, leadCurrent));
		}
	}
	incrementCount();
	return count;
}

int bucketSort(vector<int>& vect)
{
	int numOfElem = static_cast<int>(vect.size());
	AnyList* bucketArray[10];
	int compare = 0, data = 0, count = 0;

	for (int i = 0; i < 10; ++i)
	{
		bucketArray[i] = new AnyList();
	}

	for (int i = 0; i < numOfElem; ++i)
	{
		data = 0.1 * vect.at(i);
		compare += bucketArray[data]->insertInOrder(*bucketArray[data], vect.at(i));
	}

	AnyList newList;

	for (int i = 0; i < 10; ++i)
	{
		if (bucketArray[i]->getCount() != 0)
		{
			if (newList.getFirst() == nullptr)
			{
				newList.setFirst(bucketArray[i]->getFirst());
			}
			else if (newList.getFirst() != nullptr)
			{
				newList.getLast()->setNext(bucketArray[i]->getFirst());
			}
			newList.setLast(bucketArray[i]->getLast());
			newList.setCount(newList.getCount() + bucketArray[i]->getCount());
			bucketArray[i]->setFirst(nullptr);
			bucketArray[i]->setLast(nullptr);
			bucketArray[i]->setCount(0);
		}
	}

	Node* current = newList.getFirst();
	int i = 0;

	while (current != nullptr)
	{
		vect.at(i) = current->getData();
		++i;
		current = current->getNext();
	}

	for (int i = 0; i < 10; ++i)
	{
		delete bucketArray[i];
		bucketArray[i] = nullptr;
	}

	return compare;
}