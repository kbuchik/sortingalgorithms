/*
	DelRosario, Raileen
	Nguyen, Hoang
	Nguyen, Michael
	Aguilera, Kyle
	Chau, Hang

	October 30, 2019

	CS A200
	Lab 4 (Insertion Sort)
*/

#ifndef INSERTION_H
#define INSERTION_H

#include <iostream>
#include <string>
#include <vector>

using namespace std;

int insertionSort(vector<int>&);

#endif
