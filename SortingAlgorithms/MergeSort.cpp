/*
	Chhun, Michael
	Ho, Anthony
	Hoang, Huy

	October 30, 2019

	CS A200
	Lab 4 (Merge Sort)
*/

#include "MergeSort.h"

int mergeSort(vector<int>& v)
{
	int count = 0;
	v = mergeSort(v, count);
	return count;
}

vector<int> mergeSort(vector<int> m, int& count)
{
	if (m.size() <= 1)
		return m;

	vector<int> left, right, result;
	int middle = ((int)m.size() + 1) / 2;

	for (int i = 0; i < middle; i++)
	{
		left.push_back(m[i]);
	}

	for (int i = middle; i < (int)m.size(); i++)
	{
		right.push_back(m[i]);
	}

	left = mergeSort(left, count);
	right = mergeSort(right, count);
	result = merge(left, right, count);

	return result;
}

vector<int> merge(vector<int> right, vector<int> left, int& count)
{
	vector<int> result;
	while ((int)left.size() > 0 || (int)right.size() > 0)
	{
		if ((int)left.size() > 0 && (int)right.size() > 0)
		{
			if ((int)left.front() <= (int)right.front())
			{
				result.push_back((int)left.front());
				left.erase(left.begin());
			}
			else
			{
				result.push_back((int)right.front());
				right.erase(right.begin());
			}
			++count;
		}
		else if ((int)left.size() > 0)
		{
			for (int i = 0; i < (int)left.size(); i++)
				result.push_back(left[i]);
			break;
		}
		else if ((int)right.size() > 0)
		{
			for (int i = 0; i < (int)right.size(); i++)
				result.push_back(right[i]);
			break;
		}
	}
	return result;
}