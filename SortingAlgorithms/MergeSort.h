/*
	Chhun, Michael
	Ho, Anthony
	Hoang, Huy

	October 30, 2019

	CS A200
	Lab 4 (Merge Sort)
*/

#ifndef MERGESORT_H
#define MERGESORT_H

#include <iostream>
#include <vector>

using namespace std;

int mergeSort(vector<int>&);
vector<int> mergeSort(vector<int>, int&);
vector<int> merge(vector<int>, vector<int>, int&);

#endif