/*
	Nguyen, Huy
	Nguyen, Khoa
	Khong, Brian
	Chen, Linlin
	Brunelle, Taylor
	Kriesel, Michael

	October 30, 2019

	CS A200
	Lab 4 (Bucket Sort)
*/

#ifndef BUCKETSORT_H
#define BUCKETSORT_H

#include <iostream>
#include <string>
#include <vector>
#include <list>

using namespace std;

int bucketSort(vector<int>&);

int bsWrapper(vector<int>&);

class Node
{
public:
	Node() : data(0), next(nullptr) {}
	Node(int theData, Node* newNext)
		: data(theData), next(newNext) {}
	Node* getNext() const { return next; }
	int getData() const { return data; }
	void setData(int theData) { data = theData; }
	void setNext(Node* newNext) { next = newNext; }
	~Node() {}
private:
	int data;
	Node* next;
};

class AnyList
{
public:
	AnyList();
	~AnyList();

	//getter
	Node* getFirst() { return first; };
	Node* getLast() { return last; };
	int getCount() { return count; };

	//setter
	void setFirst(Node* newPtr) { first = newPtr; };
	void setLast(Node* newPtr) { last = newPtr; };
	void setCount(int newCount) { count = newCount; };
	void incrementCount() { ++count; };

	// assumption: int from 0 to 99
	int insertInOrder(AnyList& list, int elem);

private:
	Node* first;
	Node* last;
	int count;
};

#endif