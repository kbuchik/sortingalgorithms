/*
	Mangham, Cody
	Chen, Junwei
	Nguyen, Albert
	Nguyen, Nguyen

	October 30, 2019

	CS A200
	Lab 4 (Quick Sort)
*/

#include "QuickSort.h"

int quickSort(vector<int>& v1)
{
	int count = 0;

	quickSort(v1, 0, v1.size() - 1, count);

	return count;
}

void quickSort(vector<int>& v1, int h, int k, int& count) {

	if (h < k) {
		int p = partition(v1, h, k, count);

		quickSort(v1, h, p - 1, count);
		quickSort(v1, p + 1, k, count);
	}
}


int partition(vector<int>& v1, int h, int k, int& count) {
	int pivot = v1[k];
	int i = h - 1;
	for (int j = h; j <= k - 1; j++)
	{
		if (v1[j] < pivot)
		{
			i++;
			swap(v1[i], v1[j]);
		}
		count++;
	}

	swap(v1[i + 1], v1[k]);

	return i + 1;
}
