/*
	Bruhns, Eric
	Perez, Chirstian
	Katuzin-Sullivan, David
	Chao, Aaron
	Castaneda, Carlos
	Castro, Albert

	October 30, 2019

	CS A200
	Lab 4 (Selection Sort)
*/

#include "SelectionSort.h"

int selectionSort(vector<int>& v)
{
	int comp = 0;

	for (size_t j = 0; j < v.size() - 1; j++)
	{

		int min = v[j];
		int minLocation = j;
		for (size_t i = j + 1; i < v.size(); i++)
		{
			if (min > v[i])
			{
				min = v[i];
				minLocation = i;
			}
			++comp;
		}

		int temp = v[minLocation];
		v[minLocation] = v[j];
		v[j] = temp;
	}

	return comp;
}