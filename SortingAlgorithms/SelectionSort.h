/*
	Bruhns, Eric
	Perez, Chirstian
	Katuzin-Sullivan, David
	Chao, Aaron
	Castaneda, Carlos
	Castro, Albert

	October 30, 2019

	CS A200
	Lab 4 (Selection Sort)
*/

#ifndef SELECTIONSORT_H
#define SELECTIONSORT_H

#include <iostream>
#include <vector>

using namespace std;

int selectionSort(vector<int>&);

#endif