/*
	Buchik, Kevin
	Qu, Jinping
	Bui, Nam

	October 30, 2019

	CS A200
	Lab 4 (Interface)
*/

#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <vector>

#include "BubbleSort.h"
#include "BucketSort.h"
#include "InsertionSort.h"
#include "MergeSort.h"
#include "QuickSort.h"
#include "SelectionSort.h"

using namespace std;

#pragma once
class SortingInterface
{
public:
	SortingInterface();
	~SortingInterface();

	void testAll();

private:
	const int SORT_MAX = 99;

	void printVector(const vector<int>&);
	void testAlgorithm(int (*sortFunction)(vector<int>&), vector<int>&);
	int stdlibSort(vector<int>&);
	bool isSorted(const vector<int>&);
};
