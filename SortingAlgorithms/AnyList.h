// This class is used internally by the implementation of BucketSort. I've moved the code for this class to their own files to keep it separate from the bucket sort code.
// --Kevin B.

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
	Node() : data(0), next(nullptr) {}
	Node(double theData, Node* newNext)
		: data(theData), next(newNext) {}
	Node* getNext() const { return next; }
	double getData() const { return data; }
	void setData(double theData) { data = theData; }
	void setNext(Node* newNext) { next = newNext; }
	~Node() {}
private:
	double data;
	Node* next;	//pointer that points to next node
};

#pragma once
class AnyList
{
	friend std::ostream& operator<<(std::ostream&, const AnyList&);

public:
	AnyList();

	void insertBack(double);
	void insertBack(const std::vector<double>&);

	int getNumOfElem() const;

	void destroyList();
	~AnyList();

	// Declaration copy constructor
	AnyList(const AnyList&);

	// Declaration operator=
	AnyList& operator=(const AnyList&);

	//getter
	Node* getFirst() { return first; }
	Node* getLast() { return last; }
	int getCount() { return count; }

	//setter
	void setFirst(Node* newPtr) { first = newPtr; }
	void setLast(Node* newPtr) { last = newPtr; }
	void setCount(int newCount) { count = newCount; }

	// group-implemented functions
	int insertInOrder(AnyList&, int);
	int bucketSort(vector<int>&);
	//int bucketSort(vector<int>& vect);

private:
	Node* first;	//pointer to point to the first node in the list
	Node* last;		//pointer to point to the last node in the list
	int count;		//keeps track of number of nodes in the list
};

