/*
	Panella, Chris
	Qazi, Usman
	Ash, Adam
	La, Thien

	October 30, 2019

	CS A200
	Lab 4 (Bubble Sort)
*/

#include "BubbleSort.h"

int bubbleSort(vector<int>& v)
{
	int j = 0;
	bool swapDone = true;
	int size = v.size();
	int count = 0;
	while (swapDone && j < size - 1)
	{
		int temp;
		swapDone = false;
		for (int i = 0; i < (size - 1 - j); ++i)
		{
			if (v[i] > v[i + 1])
			{
				temp = v[i];
				v[i] = v[i + 1];
				v[i + 1] = temp;
				swapDone = true;
			}
			count++;
		}
		++j;
	}
	return count;
}