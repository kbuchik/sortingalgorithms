#include "AnyList.h"

// Definition of overloaded insertion stream operator as a friend function
ostream& operator<<(ostream& out, const AnyList& theList)
{
	if (theList.first == nullptr)
		out << "List is empty.";
	else
	{
		Node* temp = theList.first;

		while (temp != nullptr)
		{
			out << temp->getData() << " ";
			temp = temp->getNext();
		}
	}

	return out;
}

// constructor
AnyList::AnyList()
{
	first = last = nullptr;
	count = 0;
}

// insertBack (double)
void AnyList::insertBack(double elem)
{
	Node* newNode = new Node(elem, nullptr);

	if (first == nullptr)
		first = newNode;
	else
		last->setNext(newNode);

	last = newNode;
	++count;
}

// insertBack (vector)
void AnyList::insertBack(const vector<double>& aVector)
{
	// Assumption: Vector is non-empty.
	for (const double& i : aVector)
		insertBack(i);
}

// getNumOfElem
int AnyList::getNumOfElem() const
{
	return count;
}

// destroyList (empties the list)
void AnyList::destroyList()
{
	while (first != nullptr)
	{
		Node* temp = first;
		first = first->getNext();
		delete temp;
		temp = nullptr;
	}

	count = 0;
}

// destructor
AnyList::~AnyList()
{
	destroyList();
}

// Insert an item into the list in order
// This function was part of the solution provided by the BucketSort team for Lab 4
int AnyList::insertInOrder(AnyList& list, int elem)
{
	int count = 0;
	if (list.getFirst() == nullptr)
	{
		list.setFirst(new Node(elem, nullptr));
		list.setLast(last = first);
		++count;
	}
	else
	{
		if (list.getLast()->getData() <= elem)
		{
			list.getLast()->setNext(new Node(elem, nullptr));
			list.setLast(list.getLast->getNext());
			++count;
		}
		else if (list.getFirst()->getData() >= elem)
		{
			list.setFirst(new Node(elem, first));
			++count;
		}
		else
		{
			Node* leadCurrent = list.getFirst()->getNext(), * tailCurrent = list.getFirst();
			double x = leadCurrent->getData();
			bool found = false;

			while (!found && leadCurrent != nullptr)
			{
				if (x >= elem)
				{
					x = leadCurrent->getData();
					found = true;
				}
				else
				{
					if (leadCurrent != last)
					{
						leadCurrent = leadCurrent->getNext();
						tailCurrent = tailCurrent->getNext();
					}
					else
						found = true;
				}
			}
			tailCurrent->setNext(new Node(elem, leadCurrent));
			++count;
		}
	}
	return count;
}